package com.github.pfichtner.bytebuddypg.showcase;

public interface InterfaceWithManyMethods {

	String a();

	int b();

	Object c();

	int d();

	void e(String s);

	long f();

	int g();

	String h();

	int i();

	boolean j();

	int k();

}

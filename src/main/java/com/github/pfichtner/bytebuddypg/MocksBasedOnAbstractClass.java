package com.github.pfichtner.bytebuddypg;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.ClassLoadingStrategy;
import net.bytebuddy.dynamic.DynamicType.Loaded;
import net.bytebuddy.dynamic.DynamicType.Unloaded;

public final class MocksBasedOnAbstractClass {

	private MocksBasedOnAbstractClass() {
		super();
	}

	public static <T> T mock(Class<T> abstractClass) {
		Unloaded<? extends T> unloaded = new ByteBuddy()
				.subclass(abstractClass).make();
		Loaded<? extends T> loaded = unloaded.load(
				abstractClass.getClassLoader(),
				ClassLoadingStrategy.Default.WRAPPER);
		return newInstance(loaded.getLoaded());
	}

	private static <T> T newInstance(Class<T> clazz) {
		try {
			return clazz.newInstance();
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

}

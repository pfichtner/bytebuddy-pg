package com.github.pfichtner.bytebuddypg;

import static com.github.pfichtner.bytebuddypg.MocksBasedOnAbstractClass.mock;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.github.pfichtner.bytebuddypg.showcase.InterfaceWithManyMethods;

/**
 * Showcase for using abstract classes as "mocks" (better said stubs). No
 * Mock-Framework is in touch, {@link MocksBasedOnAbstractClass#mock(Class)}
 * will generate a new class extending the passed abstract class. Calling
 * methods not defined in the abstract class will result in
 * {@link AbstractMethodError}s (which is desired behavior).
 * 
 * @author Peter Fichtner
 */
public class ByteBuddyTest {

	// in fact it isn't just InterfaceWithManyMethods but
	// AbstractClassImplementingOnlyTheRelevantMethods (you could assign it
	// without casting)
	private final InterfaceWithManyMethods iwmm = mock(AbstractClassImplementingOnlyTheRelevantMethods.class);

	@Test
	public void testCallingMethodsDefinedInAbstractClass()
			throws InstantiationException, IllegalAccessException {
		String value = "some value " + System.currentTimeMillis();
		this.iwmm.e(value);
		assertThat(this.iwmm.h(), is(value));
	}

	@Test(expected = AbstractMethodError.class)
	public void testCallingNotImplementedMethods()
			throws InstantiationException, IllegalAccessException {
		this.iwmm.a();
	}

}

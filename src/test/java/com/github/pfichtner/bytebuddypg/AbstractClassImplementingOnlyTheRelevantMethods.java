package com.github.pfichtner.bytebuddypg;

import java.util.HashMap;
import java.util.Map;

import com.github.pfichtner.bytebuddypg.showcase.InterfaceWithManyMethods;

public abstract class AbstractClassImplementingOnlyTheRelevantMethods implements
		InterfaceWithManyMethods {

	// in contrast to Mockito's mocks based on abstract classes this works using
	// bytebuddy
	private final Map<String, Object> cache = new HashMap<String, Object>();

	public void e(String string) {
		this.cache.put("someKey", string);
	}

	public String h() {
		return (String) this.cache.get("someKey");
	}

}
